import React, {Component} from "react";

class TimerForm extends Component {
    state = {
        title: this.props.title || "",
        project: this.props.project || "",
        date: this.props.date || "",
        url: this.props.url || ""
    };

    handleTitleChange = e => {
        this.setState({title: e.target.value});
    };

    handleProjectChange = e => {
        this.setState({project: e.target.value});
    };

    handleDateChange = e => {
        this.setState({date: e.target.value});
    };

    handleURLChange = e => {
        this.setState({url: e.target.value});
    };

    handleClick = () => {
        const { title, project, date, url } = this.state;
        const id = this.props.id;

        this.props.onFormSubmit({ id, title, project, date, url })
    };

    render() {
        const submitText = this.props.title ? "Update" : "Create";
        const columnClass = !this.props.title ? "column is-3" : "column";
        const boxClass = !this.props.title ? "box" : "";

        return (
            <div className="columns is-centered">
                <div className={columnClass}>
                    <div className={boxClass}>
                        <div className="content">
                            <div className="field">
                                <label className="label">Title</label>
                                <div className="control">
                                    <input
                                        type="text"
                                        placeholder="Movie/TV Show title"
                                        value={this.state.title}
                                        onChange={this.handleTitleChange}
                                        className="input"
                                    />
                                </div>
                            </div>
                            <div className="field">
                                <label className="label">Universe</label>
                                <div className="control">
                                    <input
                                        type="text"
                                        placeholder="Brand's universe"
                                        value={this.state.project}
                                        onChange={this.handleProjectChange}
                                        className="input"
                                    />
                                </div>
                            </div>
                            <div className="field">
                                <label className="label">Date</label>
                                <div className="control">
                                    <input
                                        type="date"
                                        placeholder="Release date"
                                        value={this.state.date}
                                        onChange={this.handleDateChange}
                                        className="input"
                                    />
                                </div>
                            </div>
                            <div className="field">
                                <label className="label">YouTube's embedded URL</label>
                                <div className="control">
                                    <input
                                        type="text"
                                        placeholder="https://www.youtube.com/embed/RxAtuMu_ph4"
                                        value={this.state.url}
                                        onChange={this.handleURLChange}
                                        className="input"
                                    />
                                </div>
                            </div>
                            <div className="field is-grouped is-grouped-centered">
                                <p className="control">
                                    <button className="button is-primary" onClick={this.handleClick}>{submitText}</button>
                                </p>
                                <p className="control">
                                    <button className="button is-light" onClick={this.props.onFormCancel}>Cancel</button>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default TimerForm;