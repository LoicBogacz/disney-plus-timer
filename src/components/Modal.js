import React, {Component} from "react";

class Modal extends Component {
    onClose = e => {
        this.props.onClose && this.props.onClose(e);
    };

    render() {
        if(!this.props.show || this.props.url === "") {
            return null;
        }

        return <div className="modal is-active">
            <div className="modal-background">
            </div>
            <div className="modal-content">
                <figure className="image is-16by9">
                    <iframe title="YouTube's video player" className="has-ratio" width="1280" height="960"
                            src={this.props.url} frameBorder="0"
                            allowFullScreen>
                    </iframe>
                </figure>
            </div>
            <button className="modal-close is-large" aria-label="close" onClick={e => this.onClose(e)}>
            </button>
        </div>;
    }
}

export default Modal;