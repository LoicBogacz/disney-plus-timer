import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Button = props => {
    return (
        <div className="columns is-mobile">
            <div className="column has-text-centered">
                <button onClick={props.handleFormOpen} className="button is-large is-primary is-inverted is-outlined">
                    <FontAwesomeIcon icon="plus-circle" pull="left" /> Add a Movie or a TV Show
                </button>
            </div>
        </div>
    )
};

export default Button;