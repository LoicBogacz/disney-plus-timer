import React, { Component } from "react";

import TimerForm from "./TimerForm";
import Timer from "./Timer";

class Container extends Component {
    state = {
        isFormOpen: false
    };

    handleEditFormOpen = () => {
        this.setState({ isFormOpen: true })
    };

    handleEditFormClose = () => {
        this.setState( { isFormOpen: false })
    };

    onFormSubmit = ({ id, title, project, date, url }) => {
        this.handleEditFormClose();
        this.props.onFormSubmit({ id, title, project, date, url });
    };

    render() {
        return (
            <div className="column is-3">
                <div className="box box-tiles">
                    {this.state.isFormOpen ?
                        <TimerForm
                            id={this.props.id}
                            title={this.props.title}
                            project={this.props.project}
                            date={this.props.date}
                            url={this.props.url}
                            onFormSubmit={this.onFormSubmit}
                            onFormCancel={this.handleEditFormClose}
                        />
                        :
                        <Timer
                            id={this.props.id}
                            title={this.props.title}
                            project={this.props.project}
                            date={this.props.date}
                            url={this.props.url}
                            onEditFormOpen={this.handleEditFormOpen}
                            onEditFormClose={this.handleEditFormClose}
                            onDelete={this.props.onDelete}
                        />
                    }
                </div>
            </div>
        )
    }
}

export default Container;