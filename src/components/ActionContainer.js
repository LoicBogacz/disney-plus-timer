import React, { Component } from "react";
import PropTypes from "prop-types";

import TimerForm from "./TimerForm";
import Button from "./Button";

class ActionContainer extends Component {
    static propTypes = {
        onFormSubmit: PropTypes.func.isRequired
    };

    state = {
        isFormOpen: false
    };

    handleFormOpen = () => {
        this.setState({ isFormOpen: true })
    };

    handleFormClose = () => {
        this.setState( { isFormOpen: false })
    };

    onFormSubmit = ({ title, project, date, url }) => {
        this.handleFormClose();
        this.props.onFormSubmit({ title, project, date, url });
    };

    render() {
        if(this.state.isFormOpen) {
            return <TimerForm
                onFormSubmit={this.onFormSubmit}
                onFormCancel={this.handleFormClose}
            />
        } else {
            return <Button
                handleFormOpen={this.handleFormOpen}
            />
        }
    }
}

export default ActionContainer;