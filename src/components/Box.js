import React, { Component } from "react";
import uuid from "uuid";

import ListContainer from "./ListContainer";
import ActionContainer from "./ActionContainer";

class Box extends Component {
    state = {
        timers: [
            {
                id: "7aeb669c-affd-4d5e-8f37-o981ced401p9",
                title: "Star Wars IX",
                project: "STAR WARS",
                date: "2019-12-18",
                url: "https://www.youtube.com/embed/8Qn_spdM5Zg"
            },
            {
                id: "7aeb669c-affd-4d5e-8f37-a21aced401p9",
                title: "Black Widow",
                project: "MARVEL",
                date: "2020-04-21",
                url: "https://www.youtube.com/embed/RxAtuMu_ph4"
            },
            {
                id: "e8e0411a-4d97-483f-a647-1105674a7dpk",
                title: "The Mandalorian S2",
                project: "STAR WARS",
                date: "2020-10-01",
                url: ""
            },
            {
                id: "e8fa4c04-d48f-4278-b002-3ecead1e9b6f",
                title: "Spider-man 3",
                project: "MARVEL",
                date: "2021-07-13",
                url: ""
            }
        ]
    };

    handleCreateTimer = ({ title, project, date, url }) => {
        const timer = {
            id: uuid.v4(),
            title,
            project,
            date,
            url
        };

        this.setState({
            timers: [...this.state.timers, timer]
        })
    };

    handleEditTimer = ({ id, title, project, date, url }) => {
        this.setState({
            timers: this.state.timers.map(timer => {
                if(timer.id === id) {
                    return {
                        ...timer,
                        title,
                        project,
                        date,
                        url
                    }
                }
                return { ...timer }
            })
        })
    };

    handleDelete = id => {
        this.setState({
            timers: this.state.timers.filter(timer => timer.id !== id)
        })
    };

    render() {
        return (
            <div>
                <ListContainer
                    timers={this.state.timers}
                    onFormSubmit={this.handleEditTimer}
                    onDelete={this.handleDelete}
                />

                <ActionContainer
                    onFormSubmit={this.handleCreateTimer}
                />
            </div>
        )
    }
}

export default Box;