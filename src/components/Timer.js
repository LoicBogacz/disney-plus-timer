import React, { Component } from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

import "../helpers";
import Modal from "./Modal";

class Timer extends Component {
    state = {
        show: false
    };

    showModal = e => {
        this.setState({
            show: !this.state.show
        });
    };

    componentDidMount() {
        this.myInterval = setInterval(() => { this.forceUpdate() }, 50)
    }

    componentWillUnmount() {
        clearInterval(this.myInterval);
    }

    render() {
        const timeUntilDate = window.helpers.elapseTime(this.props.date);
        const hiddenButton = (!this.props.url.startsWith("https://www.youtube.com")) ? "button is-dark is-hidden" : "button is-dark";

        return (
            <div className="content">
                <div className="buttons is-right">
                    <button onClick={() => this.props.onDelete(this.props.id)} className="delete is-medium">Delete</button>
                </div>
                <div className="has-text-centered">
                    <div>
                        <h2 className="title">{this.props.title}</h2>
                    </div>
                    <div>
                        <p>{this.props.project}</p>
                    </div>
                    <div>
                        <h4>{timeUntilDate}</h4>
                    </div>
                </div>

                <div className="buttons is-centered">
                    <button className={hiddenButton} onClick={this.showModal}>
                        Play
                        <FontAwesomeIcon icon="play-circle" pull="right" />
                    </button>

                    <button onClick={this.props.onEditFormOpen} className="button">Update</button>
                </div>

                <Modal show={this.state.show} onClose={this.showModal} url={this.props.url} />
            </div>
        )
    }
}

export default Timer;