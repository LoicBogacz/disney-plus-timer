// Modules importation
import React, { Component } from 'react';
import Box from "./components/Box";
import './App.css'

// FontAwesome recuperation
import { library } from '@fortawesome/fontawesome-svg-core'
import { faPlusCircle, faPlayCircle, faPauseCircle } from '@fortawesome/free-solid-svg-icons'

library.add(faPlusCircle, faPlayCircle, faPauseCircle);

// Create component
class App extends Component {

    render() {
        return (
            <div className="App">
                <div className="columns">
                    <div className="column">
                        <div className="box banner-box">

                        </div>
                    </div>
                </div>

                <Box />
            </div>
        )
    }
}

// Export component to make it visible
export default App;