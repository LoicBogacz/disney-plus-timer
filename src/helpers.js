window.helpers = (function() {
  function millisecondsToHuman(ms) {
    const seconds = Math.floor((ms / 1000) % 60);
    const minutes = Math.floor((ms / 1000 / 60) % 60);
    const hours = Math.floor(ms / 1000 / 60 / 60);
    const days = Math.floor( ms / 1000 / 60 / 60 / 24);

    const humanized = [
      pad(hours.toString(), 2),
      pad(minutes.toString(), 2),
      pad(seconds.toString(), 2)
    ].join(":");

    if(days > 0) {
      return days + "d ("+humanized+")";
    } else if(hours >= 0 || minutes >= 0 || seconds >= 0) {
      return humanized;
    } else {
      return "Out!";
    }
  }

  function pad(numberString, size) {
    let padded = numberString;
    while (padded.length < size) padded = `0${padded}`;
    return padded;
  }

  function elapseTime(date) {
    const now = Date.now();
    const newDate = Date.parse(date);

    return millisecondsToHuman(newDate - now);
  }

  return {
    elapseTime
  };
})();
